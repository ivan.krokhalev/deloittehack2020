import torch
import torch.nn as nn
import torch.nn.functional as F

class CrossEntropyOnSteroids(nn.Module):

    __names__ = ['CrossEntropy']

    def __init__(
        self,
        reduction='mean',
        use_weights=False
    ):
        super().__init__()
        if use_weights:     
            self.loss_function = nn.CrossEntropyLoss(reduction=reduction, weight=torch.Tensor([1.,1.,2.]))
        else:
            self.loss_function = nn.CrossEntropyLoss(reduction=reduction)

    def forward(self, y_hat, y):
        loss_value = self.loss_function(y_hat, y)
        loss_dict = {'CrossEntropy':loss_value.item()}

        return loss_value, loss_dict
