from fastai.callbacks import *
from fastai.text import *
import numpy as np
import torch

from sklearn.metrics import accuracy_score

def accuracy_metric(y_pred, y_true, return_scores=False):
    
    y_true = y_true.detach().cpu().numpy()
    y_pred = y_pred.detach().cpu().numpy()
    
    y_pred = np.argmax(y_pred, axis=1)
    weights = np.ones_like(y_true)

    weights[y_true == 2] = 2

    score = accuracy_score(
        y_true,
        y_pred,
        sample_weight=weights
    )
    return torch.tensor(score)

def fit_lm(data, BS,  
           unfreezed_epochs=10,
           freezed_epochs=5, 
           prefix="fwd", 
           fp16 = False, 
           lr = 1e-02, 
           moms = (0.8, 0.7), 
           wd=0.1, 
           drop_mult = 0.5):
  
    lr *= BS/48
  
    learn = language_model_learner(data, AWD_LSTM, drop_mult=drop_mult,
                                   metrics=[accuracy, Perplexity()],
                               )
    if fp16: learn = learn.to_fp16()
    learn.fit_one_cycle(freezed_epochs, slice(lr), moms=moms, wd=wd)
    learn.unfreeze()
    learn.save(f"{prefix}_lm_learn_frz")
    learn = learn.load(f"{prefix}_lm_learn_frz")
    learn.fit_one_cycle(unfreezed_epochs, slice(lr/100, lr/2), moms=moms, wd=wd)
    learn.save_encoder(f"{prefix}_enc")  # to make classification, we need encoder
    learn.save(f"{prefix}_lm_model")
    return learn

def fit(data, BS, fold,
        epochs=20,
        epochs_freezed=2,
        intermediate_epoches=2,
        prefix="fwd",
        fp16 = False,
        lr = 5e-02,
        moms = (0.8, 0.7),
        wd=0.1, 
        cross_val = False):
  
    lr *= BS/48
  
    learn = text_classifier_learner(data, AWD_LSTM,
                                    pretrained=False,
                                    metrics=[accuracy_metric],
                                    )
    if fp16: learn = learn.to_fp16()
    learn.load_encoder(f"{prefix}_enc")
    learn.fit_one_cycle(epochs_freezed, lr, moms=moms, wd=wd)

    learn.freeze_to(-2)
    learn.save("learn")
    learn = learn.load("learn")
    learn.fit_one_cycle(intermediate_epoches, slice(lr/(2.6**4),lr), moms=moms, wd=wd)

    learn.freeze_to(-3)
    learn.save("learn")
    learn = learn.load("learn")
    learn.fit_one_cycle(intermediate_epoches, slice(lr/2/(2.6**4),lr/2), moms=moms, wd=wd)

    learn.unfreeze()
    learn.save("learn")
    learn = learn.load("learn")
    learn.fit_one_cycle(epochs, slice(lr/10/(2.6**4),lr/10), moms=moms, wd=wd)

    if cross_val: prefix+=str(fold)
    learn.save(f"{prefix}_class")  # saving the last model
    learn = learn.load(f"{prefix}_class")
    return learn