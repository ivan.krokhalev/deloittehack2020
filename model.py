import torch.nn as nn
import torch

from transformers import BertModel
from transformers.modeling_bert import BertPreTrainedModel

class VovaWrapper(nn.Module):
    def __init__(self, base_bert, bert_output_dim, out_dim, dropout_value):
        super().__init__()
        self.base_bert = base_bert
        
        self.classifier = nn.Sequential(
            nn.Dropout(dropout_value),
            nn.Linear(bert_output_dim, out_dim)
        )
        
    def forward(self, input_ids, attention_mask, token_type_ids):
        bert_pooled_out = self.base_bert(
            input_ids = input_ids,
            attention_mask = attention_mask,
            token_type_ids = token_type_ids
        )[1]
        
        clf_out = self.classifier(bert_pooled_out)
        
        return clf_out

class DimaBert(BertPreTrainedModel):
    def __init__(self, config):
        config.output_hidden_states = True
        super(DimaBert, self).__init__(config)
        self.num_labels = config.num_labels
        self.bert = BertModel(config)
        self.dropout = nn.Dropout(p=0.2)
        self.high_dropout = nn.Dropout(p=0.5)

        n_weights = config.num_hidden_layers + 1
        weights_init = torch.zeros(n_weights).float()
        weights_init.data[:-1] = -3
        self.layer_weights = torch.nn.Parameter(weights_init)

        self.classifier = nn.Linear(config.hidden_size, self.config.num_labels)

        self.init_weights()

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
    ):

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
        )

        hidden_layers = outputs[2]
        last_hidden = outputs[0]

        cls_outputs = torch.stack(
            [self.dropout(layer[:, 0, :]) for layer in hidden_layers], dim=2
        )
        cls_output = (torch.softmax(self.layer_weights, dim=0) * cls_outputs).sum(-1)

        # multisample dropout (wut): https://arxiv.org/abs/1905.09788
        logits = torch.mean(
            torch.stack(
                [self.classifier(self.high_dropout(cls_output)) for _ in range(5)],
                dim=0,
            ),
            dim=0,
        )

        outputs = logits
        # add hidden states and attention if they are here

        return outputs  # (loss), logits, (hidden_states), (attentions)


class VitalyiBert(BertPreTrainedModel):
    def __init__(self, config):
        config.output_hidden_states = True
        super(VitalyiBert, self).__init__(config)
        self.num_labels = config.num_labels
        self.bert = BertModel(config)
        self.dropout = nn.Dropout(p=0.2)
        self.high_dropout = nn.Dropout(p=0.5)

        self.classifier = nn.Linear(config.hidden_size * 4, self.config.num_labels)

        self.init_weights()

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
    ):

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
        )

        hidden_layers = outputs[2]
        last_hidden = outputs[0]

        cls_outputs = torch.cat(
            [self.dropout(layer[:, 0, :]) for layer in hidden_layers[-4:]], dim=1
        )

        # multisample dropout (wut): https://arxiv.org/abs/1905.09788
        logits = torch.mean(
            torch.stack(
                [self.classifier(self.high_dropout(cls_outputs)) for _ in range(5)],
                dim=0,
            ),
            dim=0,
        )

        outputs = logits
        # add hidden states and attention if they are here

        return outputs  # (loss), logits, (hidden_states), (attentions)
