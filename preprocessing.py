import argparse
import pickle
import string
import re

from collections import OrderedDict
from tqdm import tqdm
from os import path

import spacy
import pandas as pd
import torch

from transformers import BertTokenizer

from tokenization_utils import compute_input_arays

def NER(text, nlp):
    try:
        doc = nlp(text, disable=["tagger", "parser"])
        for ent in doc.ents:
            entity = str(ent)
            ent_tokens = entity.split(' ')
            taged = ''
            for ent_token in ent_tokens:
                repl = "xxx%s %s" % (ent.label_, ent_token)
                taged += repl + ' '
            text = text.replace(entity, taged)
        return text
    except:
        return text

def add_tags(tokenizer):
    spacy_tags = ['xxxperson', 'xxxnorp', 'xxxfac', 
                  'xxxorg', 'xxxgpe', 'xxxloc', 
                  'xxxproduct', 'xxxevent', 'xxxwork_of_art', 
                  'xxxlaw', 'xxxlanguage', 'xxxdate', 
                  'xxxtime', 'xxxpercent', 'xxxmoney', 
                  'xxxquantity', 'xxxordinal', 'xxxcardinal']
    for i, tag in enumerate(spacy_tags):
        tokenizer.vocab[tag] = tokenizer.vocab.pop('[unused%d]'%(i))
    return tokenizer

def heavy_clean(input_df, column_name):
    input_df[column_name] = input_df[column_name].apply(lambda x: x.lower())
    input_df[column_name] = input_df[column_name].apply(lambda text: ' '.join(list(OrderedDict.fromkeys(text.split(' ')))))
    input_df[column_name] = input_df[column_name].apply(lambda x: re.sub('\n', ' ', x) )
    input_df[column_name] = input_df[column_name].apply(lambda x: re.sub('\w*\d\w*', ' ', x) )
    input_df[column_name] = input_df[column_name].apply(lambda x: re.sub("([^\x00-\x7F])+"," ", x) )

    return input_df

def light_clean(input_df, column_name):
    input_df[column_name] = input_df[column_name].apply(lambda text: ' '.join(list(OrderedDict.fromkeys(text.split(' ')))))

    return input_df

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--df_path', type=str,
                        help='path to dataframe')
    parser.add_argument('--model_name', type=str,
                        help='name of bert model')
    parser.add_argument('--df_name', type=str,
                        default='train',
                        help='what type of df. Can be train,test or pseudo')
    parser.add_argument('--slice_strategy', type=str,
                        default='start',
                        help='slice strategy for sequances')
    parser.add_argument('--extract_target',
                        action='store_true',
                        help='whether to extract target')
    parser.add_argument('--extract_atten_mask',
                        action='store_true',
                        help='whether to attention mask')
    parser.add_argument('--extract_segment_id', 
                        action='store_true',
                        help='whether to extract segment id')
    parser.add_argument('--add_ner', 
                        action='store_true',
                        help='adds NERs tags to text and vocab. Currently do not work')
    parser.add_argument('--heavy_clean', 
                        action='store_true',
                        help='whether to use heavy preprocessing. If False light preprocessing will be used')
    args = parser.parse_args()

    df = pd.read_pickle(args.df_path)
    # Unwrap each html site into separate samples
    df = df.explode('text').reset_index(drop=True)

    if args.heavy_clean:
        df = heavy_clean(df, 'text')
    else:
        df = light_clean(df, 'text')

    
    tokenizer = BertTokenizer.from_pretrained(args.model_name)

    data_prefix = args.model_name

    if args.add_ner:
        nlp = spacy.load("en_core_web_sm")

        df['text'] = df['text'].apply(lambda text: NER(text, nlp))
            
        tokenizer = add_tags(tokenizer)

        data_prefix += '_ner'

    
    data_suffix = args.df_name

    if args.heavy_clean:
        data_suffix = data_suffix + '_heavypp'
    else:
        data_suffix = data_suffix + '_lightpp'

    # X tokenization logic
    arrays = compute_input_arays(
        df, 
        ['text', 'accepted_function', 'rejected_function', 'accepted_product', 'rejected_product'], 
        tokenizer, 
        512,
        args.slice_strategy
        )

    data_suffix = data_suffix + '_' + args.slice_strategy

    torch.save(arrays[0],path.join(path.dirname(args.df_path), data_prefix+'_'+data_suffix+'_'+'tokens_id.pt'))
    if args.extract_atten_mask:
        torch.save(arrays[1],path.join(path.dirname(args.df_path), data_prefix+'_'+data_suffix+'_'+'atten_mask.pt'))
    if args.extract_segment_id:
        torch.save(arrays[2],path.join(path.dirname(args.df_path), data_prefix+'_'+data_suffix+'_'+'segment_id.pt'))
    
    if args.extract_target:
        # Y extraction logic
        train_y = df['target']
        train_y = torch.from_numpy(train_y.values)

        torch.save(train_y,path.join(path.dirname(args.df_path), args.df_name+'_target.pt'))

    print('Completed')
    