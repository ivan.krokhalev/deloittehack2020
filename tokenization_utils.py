from math import floor, ceil
from tqdm import tqdm

import torch
import numpy as np

def _get_masks(tokens, max_seq_length):
    """Mask for padding"""
    if len(tokens)>max_seq_length:
        raise IndexError("Token length more than max seq length!")
        
    return [1]*len(tokens) + [0] * (max_seq_length - len(tokens))

def _get_segments(tokens, max_seq_length):
    """Segments: 0 for the first sequence, 1 for the second"""
    
    if len(tokens) > max_seq_length:
        raise IndexError("Token length more than max seq length!")
        
    segments = []
    first_sep = True
    current_segment_id = 0
    
    for token in tokens:
        segments.append(current_segment_id)
        if token == "[SEP]":
            if first_sep:
                first_sep = False 
            else:
                current_segment_id = 1
                
    return segments + [0] * (max_seq_length - len(tokens))

def _get_ids(tokens, tokenizer, max_seq_length):
    """Token ids from Tokenizer vocab"""
    
    token_ids = tokenizer.convert_tokens_to_ids(tokens)
    input_ids = token_ids + [0] * (max_seq_length-len(token_ids))
    return input_ids

def _trim_input(
    tokenizer, text, a_f, r_f, a_p, r_p, max_sequence_length=512, 
    slice_stratagy='use_had_tail'
    ):
        
    text = tokenizer.tokenize(text)
    a_f = tokenizer.tokenize(a_f)
    r_f = tokenizer.tokenize(r_f)
    a_p = tokenizer.tokenize(a_p)
    r_p = tokenizer.tokenize(r_p)
    
    text_len = len(text)
    a_f_len = len(a_f)
    r_f_len = len(r_f)
    a_p_len = len(a_p)
    r_p_len = len(r_p)

    if (text_len+a_f_len+r_f_len+a_p_len+r_p_len+6) > max_sequence_length:
        new_text_len = max_sequence_length - (a_f_len+r_f_len+a_p_len+r_p_len+6)
            
        if new_text_len+a_f_len+r_f_len+a_p_len+r_p_len+6 != max_sequence_length:
            raise ValueError("New sequence length should be %d, but is %d"%(max_sequence_length, (new_text_len+a_f_len+r_f_len+a_p_len+r_p_len+6)))
        
        if slice_stratagy == 'use_had_tail':
            text_len_head = round(new_text_len/2)
            text_len_tail = -1* (new_text_len -text_len_head)
            text = text[:text_len_head]+text[text_len_tail:]
        elif slice_stratagy == 'use_middle':
            pad_left = (text_len // 2) - (new_text_len // 2)
            text = text[pad_left:pad_left+new_text_len]
        elif slice_stratagy == 'start':
            text = text[:new_text_len]
        else:
            raise ValueError('Unrecognized slice strategy')

    return text, a_f, r_f, a_p, r_p

def _convert_to_bert_inputs(tokenizer, text, a_f, r_f, a_p, r_p, max_sequence_length):
    """Converts tokenized input to ids, masks and segments for BERT"""
    
    stoken = ["[CLS]"] + text + ["[SEP]"] + a_f + ["[SEP]"] + r_f + ["[SEP]"] + a_p + ["[SEP]"] + r_p + ["[SEP]"]

    input_ids = _get_ids(stoken, tokenizer, max_sequence_length)
    input_masks = _get_masks(stoken, max_sequence_length)
    input_segments = _get_segments(stoken, max_sequence_length)

    return [input_ids, input_masks, input_segments]

def _get_stoken_output(text, a_f, r_f, a_p, r_p):
    """Converts tokenized input to ids, masks and segments for BERT"""
    
    stoken = ["[CLS]"] + text + ["[SEP]"] + a_f + ["[SEP]"] + r_f + ["[SEP]"] + a_p + ["[SEP]"] + r_p + ["[SEP]"]
    return stoken

def compute_input_tokens(df, columns, tokenizer, max_sequence_length=512, slice_stratagy='use_had_tail'):

    input_tokens = []    

    for _, instance in tqdm(df[columns].iterrows()):
        t, a_f, r_f, a_p, r_p = instance.text, instance.accepted_function, instance.rejected_function, instance.accepted_product, instance.rejected_product
        t, a_f, r_f, a_p, r_p = _trim_input(tokenizer, t, a_f, r_f, a_p, r_p, max_sequence_length, slice_stratagy)
        tokens= _get_stoken_output(t, a_f, r_f, a_p, r_p)
        input_tokens.append(tokens)
        
    return input_tokens

def compute_input_arays(df, columns, tokenizer, max_sequence_length=512, slice_stratagy='use_had_tail'):
    
    input_ids, input_masks, input_segments = [], [], []
    
    for _, instance in tqdm(df[columns].iterrows()):
        t, a_f, r_f, a_p, r_p = instance.text, instance.accepted_function, instance.rejected_function, instance.accepted_product, instance.rejected_product
        t, a_f, r_f, a_p, r_p = _trim_input(tokenizer, t, a_f, r_f, a_p, r_p, max_sequence_length, slice_stratagy)
        ids, masks, segments = _convert_to_bert_inputs(tokenizer, t, a_f, r_f, a_p, r_p, max_sequence_length)

        input_ids.append(ids)
        input_masks.append(masks)
        input_segments.append(segments)
        
    return [
        torch.from_numpy(np.asarray(input_ids, dtype=np.int32)).long(), 
        torch.from_numpy(np.asarray(input_masks, dtype=np.int32)).long(),
        torch.from_numpy(np.asarray(input_segments, dtype=np.int32)).long(),
    ]
